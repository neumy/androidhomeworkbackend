# coding=utf-8

import os.path

from flask import request, jsonify, Blueprint, abort, redirect
from models.db import database
from config import IMG_ROOT

lrr = Blueprint('lrr', __name__, url_prefix='/lrr')


@lrr.route('login_check', methods=['GET'])
def login_check():
    email = request.args['email']
    password = request.args['password']

    sql = f"""
    SELECT ut.user_id, ut.email ,ut.password
    FROM user_table ut
    WHERE ut.email=\"{email}\";
    """

    users = database().select(sql)

    if len(users) == 0:
        response = {
            "status": 1
        }
        return jsonify(response)

    if users[0]["password"] != password:
        response = {
            "status": 2
        }
        return jsonify(response)
    else:
        response = {
            "status": 0
        }
        return jsonify(response)


@lrr.route('add_user', methods=["POST"])
def add_user():
    # user_id = request.args["user_id"]
    email = request.form["email"]
    password = request.form["password"]
    username = request.form["user_name"]

    sql = f"""
    SELECT ut.user_id, ut.email ,ut.password
    FROM user_table ut
    WHERE ut.email=\"{email}\";
    """

    us = database().select(sql)

    if len(us) != 0:
        response = {
            "status": 0
        }
        return jsonify(response)

    sql = f"""
    INSERT INTO user_table
    (user_name, email, password, verification_code) VALUES
    (\"{username}\", \"{email}\", \"{password}\", 114514);
    """

    database().insert(sql)

    response = {
        "status": 1
    }

    return jsonify(response)


@lrr.route('reset_password', methods=["POST"])
def reset_password():
    email = request.args['email']
    password = request.form["password"]

    sql = f"""
    UPDATE user_table
    SET password=\"{password}\"
    WHERE email=\"{email}\";
    """

    database().update(sql)

    return jsonify(
        {
            "ignore": 200
        }
    )


@lrr.route('set_code', methods=["POST"])
def set_code():
    email = request.args['email']
    code = request.form['code']

    sql = f"""
    SELECT * 
    FROM verification_table
    WHERE email=\"{email}\";
    """

    cs = database().select(sql)

    if len(cs) == 0:
        sql = f"""
        INSERT INTO verification_table
        (email, code)
        VALUES
        (\"{email}\", \"{code}\");
        """
        database().insert(sql)

    else:
        sql = f"""
        UPDATE verification_table
        SET code=\"{code}\"
        WHERE email=\"{email}\"
        """
        database().update(sql)

    return jsonify(
        {
            "ignore": 200
        }
    )


@lrr.route('code_check', methods=["GET"])
def code_check():
    email = request.args['email']
    code = request.args['code']

    sql = f"""
    SELECT *
    FROM verification_table
    WHERE email=\"{email}\" AND code=\"{code}\";
    """

    us = database().select(sql)

    if len(us) == 0:
        return jsonify({
            "status": 1
        })

    return jsonify({
        'status': 0
    })


@lrr.route('user_get', methods=["GET"])
def user_get():
    email = request.args['email']

    sql = f"""
        SELECT user_name, user_id
        FROM user_table
        WHERE email=\"{email}\";
        """

    us = database().select(sql)

    return jsonify(us)


@lrr.route('report_user', methods=["GET"])
def report_user():
    user_id = request.args['user_id']
    sql = f"""
        SELECT reported FROM user_table WHERE user_id={user_id};
    """

    old = database().select(sql)[0]["reported"]

    sql = f"""
        UPDATE user_table
        SET reported={old + 1}
        WHERE user_id={user_id};
    """

    database().update(sql)

    return jsonify({
        "status": 0
    })
