# coding=utf-8

import os.path
import datetime
import json

from models.db import database
from flask import request, jsonify, Blueprint, abort, redirect
from config import IMG_ROOT

faq = Blueprint('faq', __name__, url_prefix='/faq')


@faq.route('get_recommend_question', methods=['GET'])
def get_recommend_question():
    user_id = request.args['user_id']

    sql = "SELECT * FROM question_table qt, user_table ut WHERE qt.publisher_id=ut.user_id order BY qst_id DESC LIMIT 10;"

    response = database().select(sql)

    for qst in response:
        qst["created_time"] = qst["created_time"].strftime("%Y-%m-%d %H:%M:%S")

    return jsonify(response)


@faq.route('add_question', methods=['POST'])
def add_question():
    user_id = request.args['user_id']
    title = request.form['title']
    content = request.form['content']
    created_time = datetime.datetime.now()
    heat = 0
    look = 0

    sql = f'INSERT INTO question_table (publisher_id, created_time, title, content, heat, look) VALUES ' \
          f'({user_id}, \"{created_time}\", \"{title}\", \"{content}\", {heat}, {look});'
    response = database().insert(sql)

    if isinstance(response, tuple):
        print('插入问题时发生错误')
        return jsonify(response)

    response = {
        "status": "200"
    }
    return jsonify(response)


@faq.route('get_answer_by_qst_id', methods=['GET'])
def get_answer_by_qst_id():
    qst_id = request.args['qst_id']

    sql = f"""
        SELECT COUNT(ft.cmt_id), att.content, att.agree, att.user_name, att.ans_id, att.created_time, att.user_id
        FROM
        (
            SELECT at2.created_time, at2.content, at2.agree, ut.user_name ,at2.ans_id , ut.user_id
            FROM answer_table at2 , user_table ut
            WHERE at2.qst_id={qst_id} AND at2.publisher_id=ut.user_id
        ) AS att
        LEFT JOIN faqcomment_table ft
        ON ft.ans_id=att.ans_id
        GROUP BY att.ans_id;
        """

    response = database().select(sql)

    for ans in response:
        ans["created_time"] = ans["created_time"].strftime("%Y-%m-%d %H:%M:%S")

    return jsonify(response)


@faq.route('add_answer', methods=['POST'])
def add_answer():
    publisher_id = request.args['user_id']
    qst_id = request.args['qst_id']

    content = request.form['content']

    created_time = datetime.datetime.now()
    agree = 0

    sql = f"INSERT INTO answer_table (publisher_id, created_time, content, agree, qst_id) VALUES " \
          f"({publisher_id}, \"{created_time}\", \"{content}\", {agree}, {qst_id});"

    response = database().insert(sql)
    return jsonify(response)


@faq.route('get_comment_by_ans_id', methods=['GET'])
def get_comment_by_ans_id():
    ans_id = request.args['ans_id']

    sql = f'SELECT * FROM faqcomment_table ft, user_table ut WHERE ut.user_id=ft.publisher_id AND ft.ans_id={ans_id};'

    response = database().select(sql)

    for cmt in response:
        cmt["created_time"] = cmt["created_time"].strftime("%Y-%m-%d %H:%M:%S")

    return jsonify(response)


@faq.route('add_comment', methods=['POST'])
def add_comment():
    publisher_id = request.args['user_id']
    ans_id = request.args['ans_id']
    content = request.form['content']
    created_time = datetime.datetime.now()
    agree = 0

    sql = f"INSERT INTO faqcomment_table (publisher_id, created_time, agree, ans_id, content) VALUES " \
          f"({publisher_id}, \"{created_time}\", {agree}, {ans_id}, \"{content}\");"

    response = database().insert(sql)
    return jsonify(response)


@faq.route('subscribe_question', methods=['GET'])
def subscribe_question():
    user_id = request.args['user_id']
    qst_id = request.args['qst_id']

    sql = f"""
    INSERT INTO adrh.user_subsribe_question_maptable (user_id, qst_id) VALUES ({user_id}, {qst_id});
    """

    response = database().insert(sql)
    return jsonify(response)
