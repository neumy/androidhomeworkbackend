# coding=utf-8

import os.path

from flask import request, jsonify, Blueprint, abort, redirect
from models.db import database
from config import IMG_ROOT

msg = Blueprint('msg', __name__, url_prefix='/msg')


@msg.route('get_msg_all', methods=['GET'])
def get_msg_all_by_user_id():
    user_id = request.args['user_id']

    sql = f"""
    SELECT qt.title ,qt.qst_id
    FROM question_table qt ,subscribe_qst_table sqt 
    WHERE qt.qst_id=sqt.qst_id and sqt.user_id ={user_id};
    """

    recommend = database().select(sql)

    response = {
        "subscrib_list": [
            {
                "name": r["title"],
                "msgList_id": r["qst_id"]
            } for r in recommend
        ]
    }

    return jsonify(response)


@msg.route('get_msg_list', methods=["GET"])
def get_msg_list_by_msglist_id():
    msg_list_id = request.args["msgList_id"]

    sql = f"""
    SELECT content, created_time
    FROM msg_table 
    WHERE from_qst_id={msg_list_id};
    """

    ms = database().select(sql)

    print(ms)

    response = {
        "msg_list": [
            {
                "content": m["content"],
                "time": m['created_time']
            } for m in ms
        ]
    }

    return jsonify(response)


@msg.route('unsubscribe', methods=["POST"])
def unsubscribe():
    qst_id = request.args['qst_id']
    user_id = request.args['user_id']

    sql = f"""
    DELETE FROM subscribe_qst_table
    WHERE qst_id={qst_id} AND user_id={user_id};
    """

    database().delete(sql)

    return jsonify({
        "status": 200
    })
