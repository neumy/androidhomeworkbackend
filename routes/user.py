#coding=utf-8

import os.path

from flask import request, jsonify, Blueprint, abort, redirect
from models.db import database
from config import IMG_ROOT

user = Blueprint('user', __name__, url_prefix='/user')

@user.route('get_user_by_id', methods=['GET'])
def get_user_by_id():
    user_id = request.args['user_id']

    sql = f"select * from user_table where user_id={user_id};"

    recommend = database().select(sql)[0]

    response = recommend

    return jsonify(response)
