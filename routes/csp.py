# coding=utf-8

import os.path

from flask import request, jsonify, Blueprint, abort, redirect
from models.db import database

csp = Blueprint('csp', __name__, url_prefix='/csp')


@csp.route('get_recommend_crouse', methods=['GET'])
def get_recommend_crouse():
    user_id = request.args['user_id']

    sql = f"""
    SELECT *
    FROM crouse_table ct, department_table dt
    WHERE ct.de_id=dt.de_id
    LIMIT 1;
    """

    cs = database().select(sql)

    response = {
        "crouse_name": cs[0]["crouse_name"],
        "crouse_id": str(cs[0]['crouse_id']),
        "crouse_credits": cs[0]["credits"],
        "crouse_department": cs[0]['de_name'],
        "crouse_info": cs[0]['brief_info'],
        "crouse": cs[0]["crouse_name"],
    }

    return jsonify(response)

# TODO 没有子评论，我按评论数量排序了
@csp.route('get_crouse_list_by_heat', methods=["GET"])
def get_crouse_list_by_heat():
    sql = f"""
        SELECT *
        FROM crouse_table ct, department_table dt, 
        (
            SELECT cmt.crouse_id, COUNT(cmt.crcmt_id) heat
            FROM crcomment_table cmt 
            GROUP BY cmt.crouse_id
        ) AS cct
        WHERE ct.de_id=dt.de_id AND cct.crouse_id=ct.crouse_id 
        ORDER BY heat DESC;
        """

    cs = database().select(sql)

    response = [{
        "crouse_name": c["crouse_name"],
        "crouse_id": str(c['crouse_id']),
        "crouse_credits": c["credits"],
        "crouse_department": c['de_name'],
        "crouse_info": c['brief_info'],
        "crouse": c["crouse_name"],
    } for c in cs]
    return jsonify(response)


@csp.route("get_crouse_by_searchstr", methods=["GET"])
def get_crouse_by_search():
    searchstr = str(request.args["searchstr"])

    sql = f"""
       SELECT *
        FROM crouse_table ct, department_table dt
        WHERE ct.de_id=dt.de_id;
        """

    cs = database().select(sql)

    tmp = []
    for c in cs:
        str_id = str(c['crouse_id'])
        str_name = str(c['crouse_name'])
        if str_id == searchstr or searchstr in str_name:
            tmp.append(c)

    response = [
        {
            "crouse_name": c["crouse_name"],
            "crouse_id": str(c['crouse_id']),
            "crouse_credits": c["credits"],
            "crouse_department": c['de_name'],
            "crouse_info": c['brief_info'],
            "crouse": c["crouse_name"],
        }
        for c in tmp
    ]

    return jsonify(response)

#TODO 这里你确定url里是crouseid吗？我按你的文档写了
@csp.route("get_comment_by_crouseid", methods=["GET"])
def get_comment_by_crouseid():
    crouse_id = request.args['crouse_id']

    sql = f"""
        SELECT *
        FROM crcomment_table
        WHERE crouse_id=\"{str(crouse_id)}\";
        """

    cs = database().select(sql)

    response = [
        {
            "content": c['content'],
            "time": str(c['created_time']),
            "user_id": str(c['publisher_id']),
            "user_avator": "http://file.qqtouxiang.com/nvsheng/2019-08-02/05c81957a9c5b61bcc2c048c6ac524b1.jpg",
            "agree_count": c["agree"],
            "comment_count": 114514,
            "comment_id": c['crcmt_id']
        }
        for c in cs
    ]

    return jsonify(response)


@csp.route("post_crouse", methods=["POST"])
def post_crouse():

    print(request.args)
    print(request.form)

    user_id = request.args['user_id']
    crouse_id = request.form['crouse_id']
    crouse_name = request.form['crouse_name']
    crouse_department = request.form['crouse_department']
    crouse_credits = request.form['crouse_credits']
    #TODO 这里两个info，我只取了briefinfo
    crouse_info = request.form['crouse_info']
    crouse_briefinfo = request.form['crouse_briefinfo']

    sql = f"""
        SELECT * 
        FROM crouse_table
        WHERE crouse_id=\"{str(crouse_id)}\";
    """

    cs = database().select(sql)
    if len(cs) != 0:
        return jsonify({
            "status": 0
        })

    sql = f"""
        SELECT de_id
        FROM department_table
        WHERE de_name=\"{crouse_department}\";
        """

    ds = database().select(sql)

    if len(ds) == 0:
        sql = f"""
        INSERT INTO department_table
        (de_name)
        VALUES
        (\"{crouse_department}\");
        """

        de_id = database().insert(sql)

    else:
        de_id = ds[0]['de_id']

    sql = f"""
        INSERT INTO crouse_table
        (crouse_id, crouse_name, credits, brief_info, de_id)
        VALUES
        (\"{str(crouse_id)}\", \"{str(crouse_name)}\", {float(crouse_credits)}, \"{crouse_briefinfo}\", {int(de_id)});
        """

    database().insert(sql)

    return jsonify({
        "status": 1
    })


@csp.route("comment_crouse", methods=["POST"])
def comment_crouse():

    print(request.args)
    print(request.form)

    user_id = request.args["user_id"]
    crouse_id = request.args['crouse_id']
    content = request.form["content"]

    import datetime

    sql = f"""
        INSERT INTO crcomment_table
        (publisher_id, created_time, agree, crouse_id, content)
        VALUES
        ({int(user_id)}, \"{str(datetime.datetime.now())}\", 0, \"{str(crouse_id)}\", \"{content}\");
        """

    database().update(sql)

    return jsonify({
        "status": 200
    })


