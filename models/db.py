# coding=utf-8

import pymysql
from config import DHOST, DPORT, DUSER, DPWD, DATABASE, CHARSET

DEBUG = True

config = {
    "host": DHOST,
    "port": DPORT,
    "user": DUSER,
    "password": DPWD,
    "database": DATABASE,
    'charset': CHARSET
}


class DBInit:
    def __init__(self, data):
        try:
            self.conn = pymysql.connect(**data, autocommit=True)
            self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        except:
            self.conn.rollback()
            raise Exception("database:connection failed")

    def exec(self, sql):
        try:
            self.cursor.execute(sql)
            self.conn.commit()
        except:
            self.conn.rollback()
            raise Exception("database failed to change struct!")


class DBConnect:
    # 构造函数
    def __init__(self, data=config):
        try:
            # 获取数据库连接
            self.conn = pymysql.connect(**data)
            self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        except:
            raise Exception('database:connection failed!')

    # 增
    def insert(self, sql, data=[]):
        try:
            self.conn.ping(reconnect=True)
            self.cursor.execute(sql, data)
            self.conn.commit()
            return self.cursor.lastrowid
        except Exception as e:
            print(e)
            self.conn.rollback()
            return 0

    # 删
    def delete(self, sql, data=[]):
        try:
            self.conn.ping(reconnect=True)
            self.cursor.execute(sql, data)
            self.conn.commit()
            return self.conn.affected_rows()
        except:
            self.conn.rollback()
            return 0

    # 改
    def update(self, sql, data=[]):
        try:
            self.conn.ping(reconnect=True)
            self.cursor.execute(sql, data)
            self.conn.commit()
            return self.conn.affected_rows()
        except Exception as e:
            print(e)
            self.conn.rollback()
            return 0
        commit()

    # 查
    def select(self, sql, data=[]):
        try:
            if not data:
                self.cursor.execute(sql)
            else:
                self.cursor.execute(sql, data)
            self.conn.commit()
            data = self.cursor.fetchall()
            return data
        except Exception as e:
            print(e)
            return []

    # 查单条数据
    def get_one(self, sql, data=[]):
        try:
            self.conn.ping(reconnect=True)
            self.cursor.execute(sql, data)
            self.conn.commit()
            data = self.cursor.fetchone()
            return data
        except:
            return {}
        commit()

    # 析构函数
    def __del__(self):
        if self.cursor:
            self.cursor.close()
        if self.conn:
            self.conn.close()


def database():
    return next(generator_db())


def generator_db():
    ret = DBConnect(config)
    yield ret


if __name__ == '__main__':
    db = database()
    # sql = 'select * from question_table order by qst_id desc limit 10;'
    # print(db.select(sql))

    # 添加
    import datetime

    user_id = 1
    created_time = datetime.datetime.now()
    title = "如何给喜欢的女生挑生日礼物"
    content = "如题"
    heat = 10
    look = 9
    sql = f'INSERT INTO question_table (publisher_id, created_time, title, content, heat, look) VALUES ' \
          f'({user_id}, \"{created_time}\", \"{title}\", \"{content}\", {heat}, {look});'
    print(db.insert(sql))

    # # 删除
    # sql = "delete from user where name='小花1';"
    # print(db.delete(sql))
    #
    # # 修改
    # sql = "update user set name='{}' where id ={}".format('赵鹤',2)
    # print(db.update(sql))
