#coding=utf-8

from flask import Flask, request, jsonify
import config

app = Flask(__name__)
app.secret_key = config.SECRET_KEY

# 注册蓝图，实现模块化路由
from routes.user import user
from routes.faq import faq
from routes.msg import msg
from routes.lrr import lrr
from routes.csp import csp

app.register_blueprint(user)
app.register_blueprint(faq)
app.register_blueprint(msg)
app.register_blueprint(lrr)
app.register_blueprint(csp)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
